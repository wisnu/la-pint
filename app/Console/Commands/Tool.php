<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Tool extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'tool {exe=clear : empty} {--F|file=xaa : Filenya} {--S|sort=asc : sorted by}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		if ($this->argument('exe') == 'clear') {
			$this->clear();
		}
    }
    public function clear() {
	    shell_exec("echo '' > /var/www/laravel-pinterest/storage/logs/laravel.log");
	    shell_exec("php /var/www/laravel-pinterest/artisan cache:clear");
	    shell_exec("rm -rf /var/www/laravel-pinterest/storage/app/images/.*");
	    
    }
}
