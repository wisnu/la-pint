<?php

namespace App\Console\Commands;
use App\Keywords;
use App\User;
use Illuminate\Support\Facades\Storage;

use Illuminate\Console\Command;

class Pinterest extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'pin {exe=now : pin}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ($this->argument('exe') == 'now') {
			$this->now();
		}
		if ($this->argument('exe') == 'tes') {
			$data = json_decode(file_get_contents('https://nyc3.digitaloceanspaces.com/geneng/json/b04de6a07431cc52ac591fdfe752090e.json'));
			foreach ($data->data as $d) {
				sleep(1);

				$this->info($this->tes());
			}		
		}
	}

	public function tes() {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'ifconfig.co/ip');
		curl_setopt($curl, CURLOPT_USERAGENT, "Pinterest for iOS/5.6.1 (iPhone; 7.1.2)");
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_PROXYTYPE, 'HTTP');
		curl_setopt($curl, CURLOPT_PROXY, "83.149.70.159");
		curl_setopt($curl, CURLOPT_PROXYPORT, 13012);
		curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
		if (!$html = curl_exec($curl)) {
			$html = 'zonk';
		}
		curl_close($curl);
		return $html;
	}



	public function now() {
		sleep(rand(1,10));
		$domain = 'http://wastery.us';
		$j = json_decode(file_get_contents($domain.'/request.html'));
		$hash = $j->hash;
		\Log::info('resume '.$j->hash);
		$user = json_decode(file_get_contents($domain.'/user.html'));
		$data = json_decode(file_get_contents('https://nyc3.digitaloceanspaces.com/geneng/json/'.$j->hash.'.json'));
		$array = array();
		foreach ($data->data as $d) {
			$data = 'access_token=' . $user->access_token . '&board_id=' . $user->sample_resume . '&description=' . urlencode($j->keyword.' '.$d->content.' #sampleResume #FreeResume') . '&image_url=' . urlencode($domain.'/img/'.$d->slug.'.jpg') . '&share_facebook=0&source_url=' . urlencode($domain.'/'.str_slug($j->keyword).'/'.$d->slug.'.html');

			$result = json_decode($this->pinning($data));
			if ($result->status == 'success') {
				\Log::info('pin resume  '.$result->data->id);
				$array['data'][] = (array)$d + array('pinimg' => $result->data->image_large_url, 'pinurl' => $result->data->shareable_url);

			} elseif ($result->status == 'failure') {
				\Log::error($result->message.' - '.$data);
			}
// 			sleep(1);
		}
		Storage::disk('spaces')->put('json/'.$j->hash.'.json', json_encode($array), 'public');
		Storage::disk('local')->put('json/'.$j->hash.'.json', json_encode($array), 'public');
		shell_exec("curl -s '$domain/update/$hash.html'");
	}




	/* *
	 * pinning with same mobile data
 	 *
 	 */

	public function pinning($data) {
		$curl = curl_init();
		$header[] = "X-Pinterest-InstallId: d5f4bf89ca0a4b819c189c2ca209145f";
		$header[] = "X-Pixel-Ratio: 2";
		$header[] = "X-Pinterest-Device: iPhone3,1";
		$header[] = "X-Pinterest-AppState: active";
		curl_setopt($curl, CURLOPT_URL, 'https://api.pinterest.com/v3/pins/');
		curl_setopt($curl, CURLOPT_USERAGENT, "Pinterest for iOS/5.6.1 (iPhone; 7.1.2)");
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_PROXYTYPE, 'HTTP');
		curl_setopt($curl, CURLOPT_PROXY, "163.172.48.109");
		curl_setopt($curl, CURLOPT_PROXYPORT, 15001);
		curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
		if (!$html = curl_exec($curl)) {
			$curl2 = curl_init();
			curl_setopt($curl2, CURLOPT_URL, 'https://api.pinterest.com/v3/pins/');
			curl_setopt($curl2, CURLOPT_USERAGENT, "Pinterest for iOS/5.6.1 (iPhone; 7.1.2)");
			curl_setopt($curl2, CURLOPT_HTTPHEADER, $header);
			curl_setopt($curl2, CURLOPT_ENCODING, 'gzip,deflate');
			curl_setopt($curl2, CURLOPT_AUTOREFERER, true);
			curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl2, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl2, CURLOPT_TIMEOUT, 10);
			$html = curl_exec($curl2);
			curl_close($curl2);
		}
		curl_close($curl);
		return $html;
	}

}
