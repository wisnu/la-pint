<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use HTMLMin\HTMLMin\HTMLMin;
use HTMLMin\HTMLMin\Minifiers\BladeMinifier;
use HTMLMin\HTMLMin\Minifiers\CssMinifier;
use HTMLMin\HTMLMin\Minifiers\HtmlMinifier;
use HTMLMin\HTMLMin\Minifiers\JsMinifier;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
use League\Glide\Filesystem\FileNotFoundException;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;
use League\Glide\Signatures\SignatureException;
use League\Glide\Signatures\SignatureFactory;
use Torann\GeoIP\GeoIPFacade as GeoIP;
use App\Keywords;


class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $file;

	public function __construct() {
		// load money
		View::share('money', Config::get('money'));
	}



	/* *
	 *	Get homepage using keywords.txt file
 	 *
 	 */
	public function index() {

/*
		$location = \GeoIP::getLocation();


		if ($location['iso_code'] == 'ID') { // if visitor in Indoensia, give only 100
			$result = array_slice($this->data, 0, 120);
		} else {
			$result = $this->data;
		}
*/
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

/*
		if (Cache::has('result')) {
			$result = Cache::get('result');
		} else {			
			$result = Cache::remember('result', 1, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 0, 900);
			});
		}
*/
				shuffle($lines);
				$result = array_slice($lines, 0, 900);
		
		
		if (Cache::has('related')) {
			$related = Cache::get('related');
		} else {			
			$related = Cache::remember('related', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 1000, 21);
			});
		}



		return view('index')
		->with('result', $result)
		->with('related', $related)
		;
	}



	/* *
	 *	Render category page
 	 *
 	 *	@param string $slug
 	 */
	public function single($slug, $page=1) {
		$key = str_replace('-', ' ', $slug);
		$cachekey = md5($key);
		
// 		$keywords = new Keywords;
/*
		$exist = Keywords::where('hash', $cachekey)->first();
		if (!$exist) {
			return abort(404);

		} else {
			$result = json_decode(file_get_contents(Storage::disk('spaces')->url('json/'.$cachekey.'.json')));
			$resultArr = json_decode(file_get_contents(Storage::disk('spaces')->url('json/'.$cachekey.'.json')), true);

			if (Cache::has('random')) {
				$related = Cache::get('random');
			} else {
				$related = Cache::remember('index', 600, function() {
					return Keywords::where('status', 'pinterest')->inRandomOrder()->take(10)->get();
					
				});
			}
		}
*/
		$result = json_decode(file_get_contents(Storage::disk('local')->url('json/'.$cachekey.'.json')));
		$resultArr = json_decode(file_get_contents(Storage::disk('local')->url('json/'.$cachekey.'.json')), true);
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

		if (Cache::has('related')) {
			$related = Cache::get('related');
		} else {			
			$related = Cache::remember('related', 10, function () {
				shuffle($lines);
				return array_slice($lines, 1000, 36);
			});
		}

		if (Cache::has('related2')) {
			$related2 = Cache::get('related2');
		} else {			
			$related2 = Cache::remember('related2', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 0, 60);
			});
		}

		return view('single')
		->with('page', $page)
		->with('title', ucwords(str_replace('-', ' ', $slug)))
		->with('result', $result)
		->with('resultArr', $resultArr)
		->with('related', $related)
		->with('related2', $related2)
		->render()
		;
	}




	/* *
	 *	Render single page
 	 *
 	 *	@param string $slug
 	 *	@param string $permalink (free to change)
 	 *	@param string $id 	// base64_encode with substr (__,0,18)
 	 */
	public function attachment($slug, $permalink) {
		$key = str_replace('-', ' ', $slug);
		$cachekey = md5($key);

		$resultArr = json_decode(file_get_contents(Storage::disk('local')->url('json/'.$cachekey.'.json')), true);
		$result = json_decode(file_get_contents(Storage::disk('local')->url('json/'.$cachekey.'.json')));

		$valNum = array_search($permalink, array_column($resultArr['data'], 'slug'));
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

		if (Cache::has('related')) {
			$related = Cache::get('related');
		} else {			
			$related = Cache::remember('related', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 1000, 36);
			});
		}

		if (Cache::has('related2')) {
			$related2 = Cache::get('related2');
		} else {			
			$related2 = Cache::remember('related2', 10, function () use ($lines) {
				shuffle($lines);
				return array_slice($lines, 0, 60);
			});
		}

		return view('attachment')
		->with('id', $valNum)
		->with('slug', $slug)
		->with('result', $result)
		->with('resultArr', $resultArr)
		->with('related', $related)
		->with('related2', $related2)
		->with('suggestion', @implode(' ', $this->suggestions(str_replace('-', '+', $slug))))
		->render()
		;

	}

	public function saveImg($img_url, $imgName) {
		$manager = new ImageManager(array('driver' => 'imagick'));

		$check = get_headers($img_url, 1);
		if ($check[0] == 'HTTP/1.1 301 Found' or $check[0] == 'HTTP/1.0 301 Moved Permanently') {
			$new_url = $check['Location'];
		} else {
			$new_url = $img_url;
		}
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, $new_url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_REFERER, 'https://google.com');

		// $output contains the output string
		$output = curl_exec($ch);

		$img = $manager->make($new_url);
		$img->getCore()->stripImage();
		// save file as jpg with medium quality
		$name = $imgName.'.jpg';
		$img->save(Storage::disk('local')->url('public/'.$name), 90);
		curl_close($ch);

	}

	/* *
	 *	Static page.
 	 *
 	 *	@param string $slug

 	 */
	public function page($slug) {
		if ($slug == 'privacy-policy') {
			$pageName = 'Privacy Policy';
		} elseif ($slug == 'dmca') {
			$pageName = 'DMCA';
		} elseif ($slug == 'toc') {
			$pageName = 'Terms of Conditions';
		} elseif ($slug == 'contact') {
			$pageName = 'Contact Us';
		}

		return view('page')
		->with('page', $pageName)
		->with('content', Config::get("themes.$slug"))
		->with('related', array_slice($this->data, 0, 10))
		->render();

	}


	/* *
	 *	Helper
 	 *
 	 */
	public function suggestions($slug) {
		$relatedGo = json_decode(@file_get_contents("http://suggestqueries.google.com/complete/search?client=firefox&q=$slug&hl=en"));
		if ($relatedGo === FALSE) {
			$related = '';
			return $related;
		} elseif ($relatedGo === TRUE){
			$related = array();
			foreach ($relatedGo[1] as $r1) {
				$related[] = ucfirst($r1);
			}
	
			return $related;
		}
	}

	public function tes() {

		$json = json_decode(Storage::disk('spaces')->get('json/08bbc9cb7fcd0725e947f05d919c6403.json'),true);

		$array = $this->removeElementWithValue((array)$json['data'], 'gid', '_ow41nZSNr2HtM:');


		return json_encode($array);

	}
	public function removeElementWithValue($array, $key, $value){
		foreach($array as $subKey => $subArray){
			if($subArray[$key] == $value){
				unset($array[$subKey]);
			}
		}
		return $array;
	}

}
