<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keywords;
use App\User;
use Illuminate\Support\Facades\DB;
use Browser\Casper;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Log;

class ToolController extends Controller
{
	public function random() {

		$keyword = new Keywords;
		$keyword = $keyword->where('status', 'fetched')->inRandomOrder()->first();

		$keyword->status = 'taken';
		$keyword->save();

		return $keyword;
	}


	public function user() {
		$users = new User;

		$rand = rand(1,300);
		$user = $users->where('id', $rand)->get();
		return $user[0];
	}

	public function req() {

		$keyword = new Keywords;
		$keyword = $keyword->where('status', 'downloaded')->first();

		$keyword->status = 'pin';
		$keyword->save();

		return $keyword;
	}

	public function update($id) {
		$keyword = new Keywords;
		$keyword = $keyword->where('hash', $id)->first();
		if ($keyword) {
			return DB::connection('mysql')->table('keywords')->where('hash', $keyword->hash)->update(['status' => 'pinterest']);
		} else {
			return 'nok';
		}
	}

	/*
	*	Casper
	*
	*	@param	string	$keyword
	*/
	public function casper($keyword) {
		$casper = new Casper();
		$casper->setOptions([
			'ignore-ssl-errors' => 'yes'
			]);

		$tmpFile = storage_path('app/wallpaper/'.md5(urldecode($keyword)).'.json');
		// navigate to google web page
		$casper->start('https://www.google.com/search?&q='.$keyword.'&start=0&asearch=ichunk&tbm=isch&tbs=isz:m', $tmpFile);
		$casper->run();
		$html = json_decode(file_get_contents($tmpFile));
		$html = $html[1][1];
		$html = HtmlDomParser::str_get_html($html);

		$json['data'] = array();
		foreach ($html->find('div.rg_meta') as $a) {
			$a = json_decode($a->innertext);
			$data['gid'] = $a->id;
			$data['visibleUrl'] = $a->isu;
			$data['height'] = (int)$a->oh;
			$data['width'] = (int)$a->ow;
			$data['url'] = $a->ou;
			$data['content'] = isset($a->pt) ? $a->pt : NULL;
			$data['slug'] = str_slug($a->pt.' '.hash('crc32', $a->id, FALSE));
			$data['originalContextUrl'] = $a->ru;
			$data['title'] = isset($a->st) ? $a->st : NULL;
			$data['tbWidth'] = (int)$a->tw;
			$data['tbHeight'] = (int)$a->th;
			$data['tbUrl'] = $a->tu;
			$data['notes'] = 'inserted';

			/*
			ES::connection("wallpaper")->index("wallpaper")->type("images")->id($a->id)->insert($data);
			ES::connection("wallpaper")->index("wallpaper")->type("keywords")->id(md5(urldecode($keyword)))->insert([
				"title" => urldecode($keyword),
				"notes" => "inserted",
				"counter" => (int)0
				]);

*/
			$json['data'][] = $data;

		}
		//   Storage::disk('spaces')->put('json/'.md5(urldecode($keyword)).'.json', json_encode($json), 'public');
		//   Storage::delete('wallpaper/'.md5(urldecode($keyword)).'.json');
		unlink($tmpFile);

		//   header('Content-Type: application/json');
		return $json;
	}


}
