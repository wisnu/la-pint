@extends('main')
@section('title')
	{{ ucwords(substr(implode(' ', $result), 0, 260)) }}
@endsection

@section('meta')
<meta name="description" content="{{ ucwords(substr(implode(' ', $result), 0, 260)) }}">
<meta name="keywords" content="{{ implode(', ', $related) }}">
@endsection

@section('content')
	<div id='dd'>
		<div class="crumbs">
			<span typeof="v:Breadcrumb"><a href="/" property="v:title" rel="v:url">Home</a></span>
		</div>
		<div id="dl">
			<div class="content">
				<h1 class="ld">{{ strtoupper($result[0]) }}</h1>
				<ul>
					@foreach ($result as $data)
					<li>
						<a alt="{{ ucwords($data) }}" href="{{ url(str_slug($data)) }}" title="{{ ucwords(str_replace('-', ' ', $data)) }}">{{ substr(ucwords(str_replace('-', ' ', $data)), 0, 30) }}</a>
					</li>
					@endforeach

				</ul>
			</div>
		</div>
		<div id="sb">
			<div class="dl"></div>
			<h3 class="hc">Random post:</h3>
			<ul class="rand-text">
				@foreach ($related as $rel)
				<li>
					<h3><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a></h3>
				</li>
				@endforeach
			</ul>
			<div class="dl"></div>
		</div>
		<div class="dl"></div>
		<div id="pagination">
			<a href="{{ ('/') }}" title="First page">« First</a>...<b>1</b>
			@for ($i=2; $i<13; $i++)
				<a href="{{ ('/'.$i) }}" title="Page {{$i}}">{{$i}}</a>
			@endfor
			<a href="/13" title="Page 13">13</a>...<a href="/13" title="Last page">Last »</a>
		</div>
	</div>
@endsection