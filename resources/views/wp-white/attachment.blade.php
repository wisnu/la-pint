@extends('main')
@section('title')
	{{ ucfirst(str_replace('-', ' ', str_slug($resultArr['data'][0]['content']))) }} {{ implode(' ', array_pluck($related, 'keyword')) }}
@endsection

@section('meta')
<meta name="description" content="	{{ ucwords(str_replace('-', ' ', $slug)) }}
	@for ($i=0; $i<3; $i++)
	{{ ucwords(str_replace('-', ' ', str_slug($resultArr['data'][$i]['content']))) }},
	@endfor
">
<meta name="keywords" content="{{ ucwords(str_replace('-', ' ', $slug)) }}, {{ ucwords($resultArr['data'][0]['content']) }}">

@endsection

@section('content')
	<div id='dd'>
		<div class="crumbs">
			<span typeof="v:Breadcrumb"><a href="/" property="v:title" rel="v:url">Home</a></span> &raquo; <span class='crent' typeof="v:Breadcrumb"><a href="{{ url($slug) }}">{{ ucwords(str_replace('-', ' ', $slug)) }}</a></span>
		</div>
		<div id="dl">
			<div class="content">
				<h1 class='ld'>{{ ucwords(str_replace('-', ' ', $slug)) }}</h1>
				<div class='ads-top'>
				<?=$money['responsiveAds']; //Ads ?>
				</div>
<?php
if (isset($resultArr['data'][$id]['pinurl'])) {
	$pinUrl = $resultArr['data'][$id]['pinurl'];
	$pinImg = $resultArr['data'][$id]['pinimg'];
} else {
	$pinUrl = url('img/'.$resultArr['data'][$id]['slug']).'.jpg';
	$pinImg = $pinUrl;
}
?>
				<figure>
					<a href="{{ $pinUrl }}" alt="Free {{ ucwords($resultArr['data'][$id]['content']) }}"><img alt="{{ ucwords($resultArr['data'][$id]['content']) }}" class="attachment-full wallpaper" onerror="this.src='{{ $resultArr['data'][$id]['tbUrl'] }}'" src="{{ $pinImg }}"></a>
					<figcaption>
						{{ ucwords($resultArr['data'][$id]['content']) }}
					</figcaption>
				</figure>

				<div class='ads-top'>
					<?=$money['responsiveAds']; //Ads ?>
				</div>

		@foreach ($result->data as $result)

		<?php
			if ( !isset($result->pinimg)) {
			$pinUrl = url('img/'.$result->slug).'.jpg';
			$pinImg = $pinUrl;
		} else {
			$pinUrl = $result->pinurl;
			$pinImg = $result->pinimg;
		}
		?>

				<div class="box">
					<a class='th' href="{{ url('/'.str_slug($slug).'/'.$result->slug) }}.html" title="{{ $result->content }}"><img alt="{{ $result->content }}" height="100" onerror="null;this.src='{{ $result->tbUrl }}'" src="{{ $pinImg }}" width="225"></a>
					<h2>{{ $result->content }}</h2>
				</div>

		@endforeach

				<div style="clear:both"></div>
				<div style="clear:both"></div>
				<h3 class='ld'>Related {{ $slug }}</h3>
				<ul>
					@foreach ($related2 as $rel2)
						<li><a href="{{ url(str_slug(ucwords($rel2))) }}" title="{{ ucwords(ucwords($rel2)) }}">{{ ucwords(ucwords($rel2)) }}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
		<div id="sb">
			<div class="dl"></div>
			<div class="ads-right">
				<?=$money['responsiveAds']; //Ads ?>
			</div>
			<h3 class="hc">Random post:</h3>
			<ul class="rand-text">
				@foreach ($related as $rel)
					<li><h3><a href="{{ url(str_slug(ucwords($rel))) }}" title="{{ ucwords(ucwords($rel)) }}">{{ ucwords(ucwords($rel)) }}</a></h3></li>
				@endforeach
			</ul>
			<div class="ads-right">
				<?=$money['responsiveAds']; //Ads ?>
			</div>
			<div class="dl"></div>
		</div>
		<div class="dl"></div>
	</div>

@endsection