@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('content')
	
<header class="main-header">
<div id="header">					
		<h1 class="title single-title">{{ $query }}</h1>
</div>
</header>		
					
	<div class="post">
			
			
		@include('breadcrumb')	
			
			
		<div class="single_post">
		
			<div class="post-single-content box mark-links">
					<div class="topad">
					@yield('ads')
					</div>
					<div class="dashedpost"></div>
												
					<div class="fullimagethumb">
					<img style="width:100%"  src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['image'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}"/>
					</div>	
					
					<div class="description_images">
						<b> Description: </b><font style="text-transform: Sentence case;">{{ $query }}</font> from the above  {{ $results[0]['size'] }} resolutions which is part of the {{ $query }} directory. Download this image for free in HD resolution the choice <i>"download button"</i> below. If you do not find the exact resolution you are looking for, then go for a native or higher resolution.
					</div>
	
					<div class="ads300bottom">
						@yield('ads')
					</div>
										
				<div style="padding-top: 1px;"><h2>Detail Of {{ $query }}</h2></div>
				<div class="imgdetail">
				<p>Title : {{ $results[0]['title'] }}</p>
				<p>File Size : {{ $results[0]['size'] }}</p>
				<p>File Type : image/jpeg</p>
				</div>
				<div class="imgdetail">
				<p>Download :
				<a target="_blank" title="{{ $results[0]['title'] }}" href="{{ $results[0]['small'] }}"><u>Small Size</u></a> &#176;
				<a target="_blank" title="{{ $results[0]['title'] }}" href="{{ $results[0]['image'] }}"><u>Large Size</u></a> &#176;
				</p>
				</div>
				<div class="dashedpost"></div>
					<div class="topad">
					@yield('ads')
					</div>
					
				
				<div class="description_images_bottom">This <font style="text-transform: lowercase;">
					<a href="{{ permalink( [ $query, 0, $results[0]['title'] ], 'attachment' ) }}" title="{{ $results[0]['title'] }}" rel="bookmark">{{ $query }}</a></font> is provided only for personal use as image on computers, smartphones or other display devices. If you found any images copyrighted to yours, please contact us and we will remove it. We don't intend to display any copyright protected images.
				</div>
 						
			</div>
		<!-- End Content -->
		
		
<div class="social">
       Share on:
        <!--Twitter-->
		        <a class="twitter" href="http://twitter.com/share?text={{ $query }}&url={{ get_permalink() }}&via="title="Share on Twitter" rel="nofollow" target="_blank">Twitter</a>
		        <!--Facebook-->
		        <a class="facebook" href="http://www.facebook.com/sharer.php?u={{ get_permalink() }}&t={{ $query }}">Facebook</a>
		        <!--Google Plus-->
		        <a class="google-plus" target="_blank" href="https://plus.google.com/share?url={{ get_permalink() }}" onclick="window.open('https://plus.google.com/share?url={{ get_permalink() }}','gplusshare','width=600,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">Google+</a>
				<!--Pinterest-->
				<a class="pinterest" href="http://pinterest.com/pin/create/button/?url={{ get_permalink() }}&media={{ $results[0]['image'] }}" title="Pinterest" rel="nofollow" target="_blank">Pinterest</a>
				<!--Reddit-->
				<a class="reddit" href="http://www.reddit.com/submit?url={{ get_permalink() }}&amp;title={{ $query }}" title="Reddit" rel="nofollow" target="_blank">Reddit</a>
				<!--Stumbleupon-->
				<a class="stumbleupon" href="http://www.stumbleupon.com/submit?url={{ get_permalink() }}&amp;title={{ $query }}" title="Stumble it" rel="nofollow" target="_blank">Stumble it</a>
				<!--Digg-->
				<a class="digg" href="http://digg.com/submit?url={{ get_permalink() }}&amp;title={{ $query }}" title="Digg this!" rel="nofollow" target="_blank">Digg this!</a>
				<!--Linkedin-->
				<a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;title={{ $query }}&amp;url={{ get_permalink() }}" title="Share on LinkedIn" rel="external nofollow" rel="nofollow" target="_blank">LinkedIn</a>
				<!--Del.icio.us-->
				<a class="delicious" href="http://del.icio.us/post?url={{ get_permalink() }}&amp;title={{ $query }}" title="Bookmark on del.icio.us" rel="nofollow" target="_blank">Del.icio.us</a>
		</div>
<div class="dashedsocial"></div>
	
	
	<h3 class="related-posts2">{{ $query }}</h3>
	
			@foreach( $results as $i => $item )	
					<div class="post excerpt">
							<a href="#" rel="bookmark" title="{{ $item['title'] }}" id="featured-thumbnail">
							<div class="featured-thumbnail">
							<img src="{{ $item['small'] }}" data-src="{{ $item['image'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" alt="{{ $item['title'] }}" title="{{ $item['title'] }}">
							</div>
							<div class="featured-cat">&#9744; {{ $item['size'] }} pixel</div> </a>

							<h3 class="recenttitle">
								<a href="#" title="{{ $item['title'] }}" rel="bookmark">{{ limit_the_words($item['title'],4) }}</a>
							</h3>
					</div>		
			@endforeach
			
																
			  </div>
			  </div>

@endsection				