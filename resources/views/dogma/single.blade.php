@extends('main')
@section('title')
	[{{ $title }}]
	@for ($i=0; $i<3; $i++)
	{{ ucwords(str_replace('-', ' ', str_slug($resultArr['data'][$i]['content']))) }},
	@endfor
@endsection

@section('meta')
<meta name="description" content="{{ ucwords($title) }} {{ implode(' ', array_pluck($related, 'keyword')) }}">
<meta name="keywords" content="{{ ucwords(str_replace('-', ' ', $title)) }}, {{ implode(', ', array_pluck($related, 'keyword')) }}">


@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">

		</header>

		<div class="headertext">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" rel="v:url" property="v:title">Home</a></span> »
				<span typeof="v:Breadcrumb"></span><span class="crent">{{ $title }}</span>
			</div>
		</div>

		<div class="ads-top">
			<?=$money['responsiveAds']; //Ads ?>
		</div>
		<p>Showing {{ (($page - 1) * count($result->data) ) + 1 }} - {{ $page * count($result->data) }} of {{ rand(1300,2700) }} results from {{ $title }} images</p>
		@foreach ($result->data as $result)

		<div class="box">
			<a href="{{ url('/'.str_slug($title).'/'.$result->slug) }}.html" class="th" title="{{ $result->content }}" alt="{{ $result->content }}"><img class="th" onerror="this.onerror=null;this.src='{{ $result->pinimg }}';" src="{{ $result->pinimg }}" width="225" height="100" title="{{ $result->content }}" alt="{{ $result->content }}" oldsrc="{{ $result->pinimg }}"></a>
			<h2>{{ ucwords($result->content) }}</h2>
		</div>
		@endforeach
		<div class="clear"></div>
		<div id="pagination"><a href="{{ url('/'.str_slug($title)) }}" title="First page">« First</a>...<b>1</b>
		@for ($i=2; $i<13; $i++)
			<a href="{{ url('/'.str_slug($title).'/'.$i) }}" title="Page {{$i}}">{{$i}}</a>
		@endfor
		...<a href="{{ url('/'.str_slug($title).'/13') }}" title="Last page">Last »</a></div>

	</div>	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">

				<div id="sidebars" class="sidebar">
					<div class="ads_sidebar"><?=$money['responsiveAds']; //Ads ?><!--ads--></div>
					<div class="sidebar_list">
						<ul class="rand-text">
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug(ucwords($rel))) }}" title="{{ ucwords(ucwords($rel)) }}">{{ ucwords(ucwords($rel)) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>

						<div style="clear: both"></div>
					</div>
				</div>
			</aside>
</div>
@endsection