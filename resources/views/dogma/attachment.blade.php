@extends('main')
@section('title')
	{{ ucfirst(str_replace('-', ' ', str_slug($resultArr['data'][0]['content']))) }} {{ implode(' ', array_pluck($related, 'keyword')) }}
@endsection

@section('meta')
<meta name="description" content="	{{ ucwords(str_replace('-', ' ', $slug)) }}
	@for ($i=0; $i<3; $i++)
	{{ ucwords(str_replace('-', ' ', str_slug($resultArr['data'][$i]['content']))) }},
	@endfor
">
<meta name="keywords" content="{{ ucwords(str_replace('-', ' ', $slug)) }}, {{ ucwords($resultArr['data'][0]['content']) }}">

@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">

		</header>

		<div class="headertext">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" rel="v:url" property="v:title">Home</a></span> »
				<span typeof="v:Breadcrumb"></span><span class="crent"><a href="{{ url($slug) }}">{{ ucwords(str_replace('-', ' ', $slug)) }}</a></span> »

			</div>
		</div>

		<div class="ads-top">
			<?php echo $money['responsiveAds']; //Ads ?>
		</div>
		<?php
if (isset($resultArr['data'][$id]['pinurl'])) {
	$pinUrl = $resultArr['data'][$id]['pinurl'];
	$pinImg = $resultArr['data'][$id]['pinimg'];
} else {
	$pinUrl = url('img/'.$resultArr['data'][$id]['slug']).'.jpg';
	$pinImg = $pinUrl;
}
?>
		<figure>
			<a href="{{ $pinUrl }}" title="{{ ucwords($resultArr['data'][$id]['content']) }}"><img width="100%" src="{{ $pinImg }}" class="attachment-full wallpaper" alt="{{ ucwords(str_replace('-', ' ', str_slug($resultArr['data'][$id]['content']))) }}" title="{{ ucwords(str_replace('-', ' ', str_slug($resultArr['data'][$id]['content'])))  }}" onerror="this.src='{{ $pinImg }}';" id="exifviewer-img-0" oldsrc="{{ $pinImg }}"></a>

			<figcaption><h2>{{ ucwords(str_replace('-', ' ', str_slug($resultArr['data'][$id]['content'])))  }}. {{ $suggestion }}</h2></figcaption>
		</figure>
		<div class="ads-top">
			<?php echo $money['responsiveAds']; //Ads ?>
		</div>

		@foreach ($result->data as $result)

		<?php
			if ( !isset($result->pinimg)) {
			$pinUrl = url('img/'.$result->slug).'.jpg';
			$pinImg = $pinUrl;
		} else {
			$pinUrl = $result->pinurl;
			$pinImg = $result->pinimg;
		}
		?>

		<div class="box">
			<a href="{{ url('/'.str_slug($slug).'/'.$result->slug) }}.html" class="th" title="{{ $result->content }}" alt="{{ $result->content }}"><img class="th" onerror="this.onerror=null;this.src='{{ $pinImg }}';" src="{{ $pinImg }}" width="225" height="100" title="{{ $result->content }}" alt="{{ $result->content }}" oldsrc="{{ $pinImg }}"></a>
			<h2>{{ ucwords($result->content) }}</h2>
		</div>
		@endforeach


	</div>	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">

				<div id="sidebars" class="sidebar">
					<div class="sidebar_list">
						<ul class="rand-text">
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug(ucwords($rel))) }}" title="{{ ucwords(ucwords($rel)) }}">{{ ucwords(ucwords($rel)) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>

						<div style="clear: both"></div>
					</div>
					<div class="ads_sidebar"><?php echo $money['responsiveAds']; //Ads ?><!--ads--></div>
				</div>
			</aside>
</div>
@endsection