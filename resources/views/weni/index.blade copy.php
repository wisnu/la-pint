<html lang="en"><head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta property="og:url" content="http://themepixels.me/blockbox/templates">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Blockbox Responsive Multi-Purpose HTML5 Template">
  <meta property="og:description" content="Truly a multi-purpose and responsive html5 template that covers almost all types of UI from landing, dashboard, admin, e-commerce, apps and more. See it for yourself.">
  <meta property="og:image" content="http://www.themepixels.me/blockbox/images/cover.jpg">
  <link rel="icon" href="../images/favicon.ico">

  <title>Blockbox Blog Template</title>

  <link href="{{ theme_url('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
  <link href="{{ theme_url('lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">

  <link href="{{ theme_url('css/blockbox.css') }}" rel="stylesheet">
</head>

<body class="blog-1 tx-size-14 tx-rubik">

  <div id="navbar" class="pos-fixed-top z-index-100 bg-white pd-y-10 transition pd-md-y-25">
    <div class="container pd-xl-x-0 d-flex justify-content-between align-items-center">
      <a href="../templates/demo.html" class="tx-size-24 tx-poppins tx-bold tx-spacing-neg-4 tx-inverse">
        <i class="fa fa-cube tx-inverse"></i> block<span class="tx-medium">box</span> <span class="tx-light tx-gray">news</span>
      </a>

      <a id="showMainMenu" href="" class="tx-inverse lh-1 hidden-md-up"><i class="fa fa-navicon tx-size-22"></i></a>
      <nav class="nav nav-inverse tx-size-12 tx-uppercase tx-bold hidden-sm-down">
        <a href="" class="nav-link active">Home</a>
        <a href="" class="nav-link">Store</a>
        <a href="" class="nav-link">Sign In</a>
        <a href="" class="nav-link pd-r-0">Sign Up</a>
      </nav>
    </div><!-- container -->
  </div><!-- container-wrapper -->

  <div class="mg-t-60 mg-md-t-90">
    <div class="container pd-y-15 pd-xl-x-0 bd-y lh-7 d-md-flex justify-content-start align-items-start align-items-lg-center">

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- link_728 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:15px"
     data-ad-client="ca-pub-8355539896336610"
     data-ad-slot="6081129834"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

    </div><!-- container -->
  </div><!-- container-wrapper -->

  <div class="pd-y-30">
    <div class="container pd-xl-x-0">
      <div class="row row-lg">
        <div class="col-lg-8 ">
          <div class="row">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-8355539896336610"
     data-ad-slot="8421897301"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		  	@for ($r=1; $r<11; $r++)
		  		@php
		  			$json = json_decode(file_get_contents(Storage::disk('local')->url('json/'.md5($result[$r]).'.json')));
		  		@endphp
			  <h2 class="tx-inverse mg-b-35">{{ $json->data[0]->content}}</h2>
			  <img src="{{ $json->data[0]->pinimg }}" class="img-fluid mg-b-35" alt="">
			  	@for ($i=1; $i < 5; $i++)
			  		<div class="col-lg-3">
			  			<a href="blog-single-1.html">
			                <figure class="effect-fade">
			                  <img src="{{ $json->data[$i]->pinimg }}" alt="" width="188px" height="200px">
			                </figure>
						</a>
							<h6 class="mg-t-20 tx-bold"><a href="blog-single-1.html" class="tx-gray-dark lh-4">{{ $json->data[$i]->content }}</a></h6>
				  	</div>
				@endfor
		  	@endfor
		   </div><!-- row -->


          


        </div><!-- col-9 -->


        <div class="col-lg-4">
          <h6 class="tx-inverse tx-merriweather tx-bold mg-b-0">Popular News</h6>
          <hr class="mg-y-20">
          <div class="side-list">
            <div class="side-list-item">
              <h6 class="tx-size-14 tx-bold lh-4"><a href="blog-single-1.html" class="tx-bold tx-gray-dark">40 Most Useful Travel Websites That Can Save You a Fortune</a></h6>
              
              
            <hr class="mg-y-20"><h6 class="tx-size-14 tx-bold lh-4"><a href="blog-single-1.html" class="tx-bold tx-gray-dark">40 Most Useful Travel Websites That Can Save You a Fortune</a></h6></div>

            

            

            

            

            

            

            <hr class="mg-y-20">

            <div class="side-list-item">
              <a href="blog-single-1.html">
                <figure class="effect-fade">
                  <img src="../images/img326.jpg" class="img-fluid" alt="">
                </figure>
              </a>
              <h6 class="tx-size-14 tx-bold lh-4 mg-t-20"><a href="blog-single-1.html" class="tx-bold tx-gray-dark">The Most Common Budgeting Mistakes and How to Fix Them</a></h6>
              <div class="d-flex justify-content-between align-items-center tx-size-12 mg-b-15">
                <span>By: Dean M. Lowery</span>
                <span>Aug 20, 2017</span>
              </div>
              <p class="tx-size-12">When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the...</p>
            </div><!-- side-list-item -->
          </div><!-- side-list -->

          <h6 class="tx-inverse tx-merriweather tx-bold mg-b-0 mg-t-60">Hot News</h6>
          <hr class="mg-y-20">
          <!-- side-list -->
        </div><!-- col-3 -->
      </div><!-- row -->
    </div><!-- container -->
  </div><!-- container-wrapper -->

  <div class="container-wrapper">
    <div class="container pd-xl-x-0 bd-t pd-y-25 tx-size-12 d-md-flex justify-content-between">
      <div class="wd-md-60p">
        <a href="../templates/demo.html" class="d-block mg-b-5 tx-size-24 tx-poppins tx-bold tx-spacing-neg-4 tx-inverse">
          <i class="fa fa-cube tx-inverse"></i> block<span class="tx-medium">box</span> <span class="tx-light tx-gray">news</span>
        </a>
        <p class="tx-size-12 mg-b-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using.</p>
      </div>
      <nav class="nav nav-inline nav-gray tx-size-24">
        <a href="" class="nav-link pd-l-0 pd-md-l-15 pd-x-5"><i class="fa fa-facebook"></i></a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15 pd-x-5"><i class="fa fa-twitter"></i></a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15 pd-x-5"><i class="fa fa-pinterest"></i></a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15 pd-x-5"><i class="fa fa-youtube"></i></a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15 pd-x-5"><i class="fa fa-github"></i></a>
      </nav>
    </div><!-- container -->

    <div class="tx-size-12 container pd-xl-x-0 d-lg-flex justify-content-between align-items-center">
      <nav class="nav nav-inline nav-gray flex-column flex-sm-row">
        <a href="" class="nav-link pd-l-0">Home</a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15">Explore</a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15">About</a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15">Terms of Use</a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15">Privacy Policy</a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15">Affiliates</a>
        <a href="" class="nav-link pd-l-0 pd-md-l-15">Contact Us</a>
      </nav>
      <span class="d-inline-block mg-t-15 mg-sm-t-0">© 2017 Blockbox News. All Rights Reserverd.</span>
    </div><!-- container -->

  </div><!-- container-wrapper -->


  <script src="{{ theme_url('lib/jquery/jquery.js') }}"></script>
  <script src="{{ theme_url('lib/popper.js/popper.js') }}"></script>
  <script src="{{ theme_url('lib/bootstrap/bootstrap.js') }}"></script>
  <script>

  'use strict';

  // header scroll animation effect
  $(window).scroll(function() {
    if ($(document).scrollTop() > 80) {
      $('#navbar').addClass('shadow-base');
      $('#navbar').removeClass('pd-md-y-25');
    } else {
      $('#navbar').removeClass('shadow-base');
      $('#navbar').addClass('pd-md-y-25');
    }
  });

  //show main menu
  // for mobile only
  $('#showMainMenu').on('click', function(e){

    if($('#mainMenu').is(':visible')) {
      $('#mainMenu').addClass('d-none');
    } else {
      $('#mainMenu').removeClass('d-none');
    }
    return false;
  });
  </script>



</body></html>